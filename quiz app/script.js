const quizData = [
    {
      question: 'Who has written the book " THE ALCHEMIST"',
      a: 'Paulo Coelho',
      b: 'Charles',
      c: 'George Orwell' ,
      d: 'Khalil Gibran',
      correct: 'a' 
    },
    {
        question: 'Which is the pink city in India?',
        a: 'Jaipur',
        b: 'Kanpur',
        c: 'Belagavi' ,
        d: 'Pune',
        correct: 'a'   
    },
    {
        question: 'Which is the garden city in India?',
        a: 'Mysore',
        b: 'Bangalore',
        c: 'Lucknow' ,
        d: 'Pune',
        correct: 'b'   
    },
    {
        question: 'Anne Frank is from which place',
        a: 'Amsterdam',
        b: 'Germany',
        c: 'France' ,
        d: 'USA',
        correct: 'a'   
    },
    
    {
        question: 'Which city is called land of Gods in India?',
        a: 'Manali',
        b: 'Assam',
        c: 'Kerala',
        d: 'Udupi',
        correct: 'c'   
    },
    
    
];
const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");

let currentQuiz = 0;
let score = 0;

loadQuiz();

function loadQuiz() {
    deselectAnswers();

    const currentQuizData = quizData[currentQuiz];

    questionEl.innerText = currentQuizData.question;
    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;
}

function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });

    return answer;
}

function deselectAnswers() {
    answerEls.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn.addEventListener("click", () => {
    // check to see the answer
    const answer = getSelected();

    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }

        currentQuiz++;
        if (currentQuiz < quizData.length) {
            loadQuiz();
        } else {
            quiz.innerHTML = `
                <h2>You answered correctly at ${score}/${quizData.length} questions.</h2>
                
                <button onclick="location.reload()">Reload</button>
            `;
        }
    }
});
